"use strict"

let num1 = '';
let num2 = '';
let sign = ''; 
let finish = false;
let memory = '';
let count = 0;

const readonly = document.querySelector('.readonly');
const clear = document.querySelector('.clear');
const keys = document.querySelector('.keys');
const shoeMem = document.querySelector('.memory');

const clearAll = () => {
    num1 = '';
    num2 = '';
    sign = '';
    finish = false;
    readonly.value = '0';
}

clear.addEventListener('click', e => {
    clearAll();
})

keys.addEventListener('click', e => {
    if (e.target === clear) return;
    if (!e.target.classList.contains('button')) return;
    if (e.target.classList.contains('black')) {
        if (num2 === '' && sign === '') {
            num1 += e.target.value;
            readonly.value = num1;
        } else if (num1 !== '' && num2 !== '' && finish) {
            finish = false;
            num2 = e.target.value;
            readonly.value = num2;
        } else {
            num2 += e.target.value;
            readonly.value = num2;
        } return;
    }
    if (e.target.classList.contains('pink')) {
        sign += e.target.value;
        return;
    }
    if (e.target.classList.contains('orange')) {

        switch (sign) {
            case '+':
                num1 = (+num1) + (+num2);
                break;
            case '-':
                num1 = (+num1) - (+num2);
                break;
            case '*':
                num1 = (+num1) * (+num2);
                break;
            case '/':
                num1 = (+num1) / (+num2);
                break;
        }
        readonly.value = num1;
        sign = '';
        finish = true;
        return;
    }
    if (e.target.classList.contains('gray')) {
        if (e.target.classList.contains('memory')) {
            memory = readonly.value;
            shoeMem.value = 'm';
            console.log(memory)
            return;
        }
        if (e.target.classList.contains('mrc')) {
            count +=1;
            if (count === 1) {
                if (num1 !== '') {
                    num2 = memory;
                    readonly.value = num2; 
                   } else {
                    num1 = memory;
                    readonly.value = num1; 
                   }  
            } else if (count ===2) {
                memory = '';
                num2 = 0;
                shoeMem.value = '';
                console.log(memory)
            } 
            console.log(memory);
            
        }return;
    }
    
})
